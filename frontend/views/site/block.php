<?php

/* @var $this yii\web\View */
use yii\bootstrap\Html;

/* @var $block common\models\Block */


$this->title = 'Блок "' . Html::encode($block->name) . '"';
?>
<div class="site-block">

    <h4>Блок: <?= $this->title ?></h4>

    <table>
        <tr>
            <td class="td-name">ID</td>
            <td class="td-val"><?= $block->id ?></td>
        </tr>
        <tr>
            <td class="td-name">Название блока</td>
            <td class="td-val"><?= Html::encode($block->name) ?></td>
        </tr>
        <tr>
            <td class="td-name">Создан</td>
            <td class="td-val"><?= Html::encode($block->created_at) ?></td>
        </tr>
        <tr>
            <td class="td-name">Родительская категория</td>
            <td class="td-val"><?= Html::encode($block->getCategory()->one()->name) ?></td>
        </tr>
    </table>
</div>
