<?php

/* @var $this yii\web\View */
/* @var $categories common\models\Category[] */

$this->title = 'Главные категории';
?>
<div class="site-index">

    <h4>Список категорий:</h4>
    <ul>

    <?php foreach ($categories as $category): ?>

        <li><?= \yii\helpers\Html::a($category->name, ['/site/category', 'category_id' => $category->id]) ?></li>

    <?php endforeach ?>

    </ul>

</div>
