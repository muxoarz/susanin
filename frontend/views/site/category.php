<?php

/* @var $this yii\web\View */
use yii\helpers\Html;

/* @var $current_category common\models\Category */
/* @var $categories common\models\Category[] */

$this->title = 'Подкатегории "' . Html::encode($current_category->name) . '"';
?>
<div class="site-index">

    <h4>Список подкатегорий:</h4>
    <ul>

    <?php foreach ($categories as $category): ?>

        <li>
            <?= Html::a($category->name, ['category', 'category_id' => $category->id]) ?>
        </li>

    <?php endforeach ?>

    </ul>


    <h4>Список блоков:</h4>
    <ul>

        <?php foreach ($current_category->getShowBlocks() as $block): ?>

            <li>
                <?= Html::a($block->name, ['/site/block', 'alias' => $block->alias]) . ' <i>' . $block->created_at . '</i>' ?>
            </li>

        <?php endforeach ?>

    </ul>
</div>
