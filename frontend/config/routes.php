<?php

return [
    'block/<alias>' => '/site/block',
    'category/<category_id:\d+>' => '/site/category'
];