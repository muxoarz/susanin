<?php

use yii\db\Migration;

class m160225_074933_create_category_block_shows extends Migration
{
    public function up()
    {
        $this->createTable('category_block_shows', [
            'category_id' => $this->integer()->notNull(),
            'block_id' => $this->integer()->notNull()
        ]);
        $this->addPrimaryKey('category_block_shows_pk', 'category_block_shows', ['category_id', 'block_id']);
        $this->addForeignKey(
            'category_block_shows_category_id_fk',
            'category_block_shows', 'category_id',
            'category', 'id',
            'CASCADE', 'CASCADE'
        );
        $this->addForeignKey(
            'category_block_shows_block_id_fk',
            'category_block_shows', 'block_id',
            'block', 'id',
            'CASCADE', 'CASCADE'
        );
    }

    public function down()
    {
        $this->dropTable('category_block_shows');
    }
}
