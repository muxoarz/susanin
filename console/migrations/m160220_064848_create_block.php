<?php

use yii\db\Migration;

class m160220_064848_create_block extends Migration
{
    public function up()
    {
        $this->createTable('block', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer()->notNull(),
            'name' => $this->string(100)->notNull(),
            'alias' => $this->string(100)->notNull()->unique(),
            'is_active' => $this->boolean()->notNull()->defaultValue(true),
            'created_at' => $this->timestamp()->defaultExpression('now()')
        ]);
        $this->addForeignKey(
            'block_category_id_fk',
            'block', 'category_id',
            'category', 'id',
            'CASCADE', 'CASCADE'
        );
        $this->createIndex(
            'block_category_id_index',
            'block',
            ['category_id', 'is_active']
        );
    }

    public function down()
    {
        $this->dropTable('block');
    }
}
