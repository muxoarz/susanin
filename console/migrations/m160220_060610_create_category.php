<?php

use yii\db\Migration;

class m160220_060610_create_category extends Migration
{
    public function up()
    {
        $this->createTable('category', [
            'id' => $this->primaryKey(),
            'parent_id' => $this->integer(),
            'name' => $this->string(50)->notNull(),
            'is_active' => $this->boolean()->notNull()->defaultValue(true)
        ]);
        $this->addForeignKey(
            'category_parent_id_fk',
            'category', 'parent_id',
            'category', 'id',
            'CASCADE', 'CASCADE'
        );
        $this->createIndex(
            'category_parent_id_index',
            'category',
            ['parent_id', 'is_active']
        );
    }

    public function down()
    {
        $this->dropTable('category');
    }
}
