<?php

use yii\db\Migration;

class m160220_065214_create_block_fields extends Migration
{
    public function up()
    {
        $this->createTable('block_fields', [
            'id' => $this->primaryKey(),
            'block_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'type_id' => $this->integer()->notNull(),
            'value' => $this->text(),
            'is_active' => $this->boolean()->notNull()->defaultValue(true)
        ]);
        $this->addForeignKey(
            'block_fields_block_id_fk',
            'block_fields', 'block_id',
            'block', 'id',
            'CASCADE', 'CASCADE'
        );
        $this->createIndex(
            'block_fields_block_id_index',
            'block_fields',
            ['block_id', 'is_active']
        );
    }

    public function down()
    {
        $this->dropTable('block_fields');
    }
}
