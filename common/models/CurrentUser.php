<?php
namespace common\models;

use common\models\User;
use Yii;
use yii\base\Object;

class CurrentUser extends Object
{
    private static $user;
    public function get()
    {
        if (Yii::$app->user->isGuest) {
            return null;
        }
        if (self::$user instanceof User) {
            return self::$user;
        } else {
            self::$user = User::findOne(Yii::$app->user->id);
            return self::$user;
        }
    }
}