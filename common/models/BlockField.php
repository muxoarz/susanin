<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "block_fields".
 *
 * @property integer $id
 * @property integer $block_id
 * @property string $name
 * @property integer $type_id
 * @property string $value
 * @property integer $is_active
 *
 * @property Block $block
 */
class BlockField extends \yii\db\ActiveRecord
{
    const TYPE_STRING = 1;
    const TYPE_IMAGE = 2;
    const TYPE_LIST = 3;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'block_fields';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['block_id', 'name', 'type_id'], 'required'],
            [['block_id', 'type_id', 'is_active'], 'integer'],
            [['value'], 'string'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'block_id' => 'Block ID',
            'name' => 'Name',
            'type_id' => 'Type ID',
            'value' => 'Value',
            'is_active' => 'Is Active',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlock()
    {
        return $this->hasOne(Block::className(), ['id' => 'block_id']);
    }

    /**
     * Список названий типов полей
     * @return array
     */
    public static function typeNames()
    {
        return [
            self::TYPE_STRING => 'Строка',
            self::TYPE_IMAGE => 'Картинка',
            self::TYPE_LIST => 'Список',
        ];
    }
}
