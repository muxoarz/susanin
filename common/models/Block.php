<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "block".
 *
 * @property integer $id
 * @property integer $category_id
 * @property string $name
 * @property string $alias
 * @property integer $is_active
 * @property string $created_at
 *
 * @property Category $category
 * @property BlockFields[] $blockFields
 */
class Block extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'block';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'name', 'alias'], 'required'],
            [['category_id', 'is_active'], 'integer'],
            [['name', 'alias'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Категория',
            'name' => 'Название',
            'alias' => 'Алиас',
            'is_active' => 'Вкл/Выкл',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlockFields()
    {
        return $this->hasMany(BlockFields::className(), ['block_id' => 'id']);
    }


    /**
     * Привязывает категории к блоку для показа в них
     * @param array $categories массив айди категорий для показа блока
     * @return bool
     */
    public function saveCategoriesShows(array $categories)
    {
        CategoryBlockShow::deleteAll(['block_id' => $this->id]);
        foreach ($categories as $category_id) {
            $cbs = new CategoryBlockShow();
            $cbs->category_id = $category_id;
            $cbs->block_id = $this->id;
            $cbs->save();
        }
        return true;
    }
}
