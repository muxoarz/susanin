<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $name
 * @property integer $is_active
 *
 * @property Block[] $blocks
 * @property Category $parent
 * @property Category[] $categories
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'is_active'], 'integer'],
            [['name'], 'required'],
            [['name'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Родительская категория',
            'name' => 'Название категории',
            'is_active' => 'Вкл/Выкл',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlocks()
    {
        return $this->hasMany(Block::className(), ['category_id' => 'id']);
    }

    /**
     * Блоки которые показыааются в категории
     * @return Block[]
     */
    public function getShowBlocks()
    {
        return Block::findBySql('SELECT * FROM block WHERE id
            IN (SELECT block_id FROM category_block_shows WHERE category_id=:category_id)
            AND is_active=true',
            [':category_id' => $this->id])->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Category::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['parent_id' => 'id']);
    }

    /**
     * Выводит дерево категорий (для dropDownList)
     * @param null   $parent_category_id
     * @param string $offset
     * @return array
     */
    public static function getCategoriesTree($parent_category_id = null, $offset = '')
    {
        $categories = [];
        /** @var Category $category */
        foreach (Category::find()->where(['parent_id' => $parent_category_id])->orderBy('name')->all() as $category) {
            $categories[$category->id] = $offset . $category->name;
            $categories = ArrayHelper::merge($categories, self::getCategoriesTree($category->id, $offset . '....'));
        }
        return $categories;
    }
}
