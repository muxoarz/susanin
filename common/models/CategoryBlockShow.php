<?php

namespace common\models;

use Yii;

/**
 * Перевязка категорий и блоков для показа на сайте
 *
 * @property integer $category_id
 * @property integer $block_id
 *
 * @property Block $block
 * @property Category $category
 */
class CategoryBlockShow extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category_block_shows';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'block_id'], 'required'],
            [['category_id', 'block_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'category_id' => 'Категория',
            'block_id' => 'Блок',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlock()
    {
        return $this->hasOne(Block::className(), ['id' => 'block_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }
}
