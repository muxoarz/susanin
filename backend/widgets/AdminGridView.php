<?php

namespace backend\widgets;

use yii\grid\GridView;

class AdminGridView extends GridView {
    public function init() {
        parent::init();
        $this->layout = '<div class="box"><div class="box-body table-responsive no-padding">{pager}{summary}{items}{pager}</div></div>';
    }
}