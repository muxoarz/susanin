<?php
/**
 * Created by PhpStorm.
 * User: muxo
 * Date: 11/9/15
 * Time: 11:36 AM
 */

namespace backend\widgets;

use yii\widgets\ActiveForm;

class AdminActiveForm extends ActiveForm {

    public function init() {
        parent::init();
        $this->fieldConfig = [
            'template' => '{label}{input}{error}
                                <span class="help-block">{hint}</span>',
            'labelOptions' => ['class' => 'control-label'],
            'inputOptions' => ['class' => "form-control"],
        ];
        $this->options['class'] = 'form-horizontal';
    }
}