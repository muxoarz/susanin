<?php

namespace backend\widgets;

use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class AdminMenu extends \dmstr\widgets\Menu
{
    public $linkTemplate = '<a href="{url}">{icon} {label} {count}</a>';

    protected function renderItem($item)
    {
        if(isset($item['items']))
            $linkTemplate = '<a href="{url}">{icon} {label} <i class="fa fa-angle-left pull-right"></i></a>';
        else
            $linkTemplate = $this->linkTemplate;
        if (isset($item['url'])) {
            $template = ArrayHelper::getValue($item, 'template', $linkTemplate);
            $replace = !empty($item['icon']) ? [
                '{url}' => Url::to($item['url']),
                '{label}' => '<span>'.$item['label'].'</span>',
                '{icon}' => '<i class="' . $item['icon'] . '"></i> '
            ] : [
                '{url}' => Url::to($item['url']),
                '{label}' => '<span>'.$item['label'].'</span>',
                '{icon}' => null,
            ];
            $replace['{count}'] = isset($item['count']) ? '<small class="label pull-right bg-green">'.$item['count'].'</small>' : null;
            return strtr($template, $replace);
        } else {
            $template = ArrayHelper::getValue($item, 'template', $this->labelTemplate);
            $replace = !empty($item['icon']) ? [
                '{label}' => '<span>'.$item['label'].'</span>',
                '{icon}' => '<i class="' . $item['icon'] . '"></i> '
            ] : [
                '{label}' => '<span>'.$item['label'].'</span>',
            ];
            return strtr($template, $replace);
        }
    }
}