<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Block */

$this->title = 'Редактирование блока: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Все блоки', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
?>
<div class="block-update">



    <?= $this->render('_form', [
        'model' => $model,
        'category_shows' => $category_shows
    ]) ?>

</div>
