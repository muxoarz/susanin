<?php

use backend\widgets\AdminActiveForm;
use common\models\Category;
use yii\helpers\Html;

$categories = Category::getCategoriesTree();

/* @var $this yii\web\View */
/* @var $model common\models\Block */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="block-form">

    <div class="box box-info">

    <?php $form = AdminActiveForm::begin(); ?>

        <div class="box-body">

            <?= $form->field($model, 'category_id')->dropDownList($categories) ?>

            <?= $form->field($model, 'name')->textInput(['maxlength' => 100]) ?>

            <?= $form->field($model, 'alias')->textInput(['maxlength' => 100]) ?>

            <?= $form->field($model, 'is_active')->radioList([true => 'Вкл.', false => 'Выкл.']) ?>

            <div class="form-group required">
                <label class="control-label" for="block-category_shows">Показывать в категориях</label><input type="hidden" name="Block[show_category_id]" value="">
                <div id="block-category_shows">
                    <?php foreach ($categories as $cat_id => $cat_name): ?>
                        <div><label><?= Html::checkbox('Block[category_shows]['.$cat_id.']', isset($category_shows[$cat_id])) ?> <?= Html::encode($cat_name) ?></label></div>
                    <?php endforeach ?>
                <span class="help-block"></span>
            </div>

        </div>

        <div class="box-footer">
            <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php AdminActiveForm::end(); ?>

    </div>

</div>
