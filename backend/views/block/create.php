<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Block */

$this->title = 'Создание блока';
$this->params['breadcrumbs'][] = ['label' => 'Все блоки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="block-create">

    <?= $this->render('_form', [
        'model' => $model,
        'category_shows' => $category_shows
    ]) ?>

</div>
