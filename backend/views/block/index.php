<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Все информационные блоки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="block-index">



    <p>
        <?= Html::a('Создать блок', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'category.name:text:Категория',
            'name',
            'alias',
            [
                'attribute' => 'Вкл/Выкл',
                'format'=>'raw',
                'value' => function($data) {
                    /** @var Category $data */
                    if ($data->is_active) {
                        return '<strong>Вкл</strong>';
                    }
                    return 'Выкл';
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
