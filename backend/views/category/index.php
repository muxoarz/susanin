<?php

use backend\widgets\AdminGridView;
use common\models\Block;
use common\models\Category;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Все категории';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">



    <p>
        <?= Html::a('Создать категорию', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= AdminGridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'Родительская категория',
                'format'=>'raw',
                'value' => function($data) {
                    /** @var Category $data */
                    if (!is_null($data->parent_id)) {
                        return Html::encode(Category::findOne(['id' => $data->parent_id])->name);
                    }
                    return '';
                },
            ],
            'name',
            [
                'attribute' => 'Вкл/Выкл',
                'format'=>'raw',
                'value' => function($data) {
                    /** @var Category $data */
                    if ($data->is_active) {
                        return '<strong>Вкл</strong>';
                    }
                    return 'Выкл';
                },
            ],
            [
                'attribute' => 'Информационных блоков',
                'format'=>'raw',
                'value' => function($data) {
                    /** @var Category $data */
                    return Html::a(Block::find()->where(['category_id' => $data->id])->count(), ['/block', 'category_id' => $data->id]);
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
