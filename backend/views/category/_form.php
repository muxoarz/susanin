<?php

use backend\widgets\AdminActiveForm;
use common\models\Category;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$categories = ArrayHelper::merge([null => 'Нет родителя(верхний уровень)'], Category::getCategoriesTree());
// нельзя привязать категорию к себе же
if ($model->id !== null) {
    unset($categories[$model->id]);
}

/* @var $this yii\web\View */
/* @var $model common\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-form">

    <div class="box box-info">

    <?php $form = AdminActiveForm::begin(); ?>

        <div class="box-body">

        <?= $form->field($model, 'parent_id')->dropDownList($categories) ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => 100]) ?>

        <?= $form->field($model, 'is_active')->radioList([true => 'Вкл.', false => 'Выкл.']) ?>

        </div>

    <div class="box-footer">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php AdminActiveForm::end(); ?>

    </div>

</div>
