<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Логин';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">


    <p>Заполните поля для входа на сайт:</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = AdminActiveForm::begin(['id' => 'login-form']); ?>

                <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

                <?= $form->field($model, 'rememberMe')->checkbox() ?>

                <div class="form-group">
                    <?= Html::submitButton('Логин', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>

            <?php AdminActiveForm::end(); ?>
        </div>
    </div>
</div>
